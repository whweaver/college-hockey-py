from django.db import models
import os
import pathlib
from datetime import datetime, timezone
import hashlib

class File(models.Model):
	location = models.CharField(max_length=1024, null=False, unique=True)
	last_updated = models.DateTimeField(null=True)
	md5 = models.CharField(max_length=255, null=True)

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def create(cls, location):
		"""Factory method to create a File object from only a filepath.
		Positional arguments:
		location -- The path to the file in the filesystem.
	
		Returns the created File object.
		"""
		last_updated, md5 = cls._defaults()
		return cls(location=location, last_updated=last_updated, md5=md5)

	@classmethod
	def get_or_create(cls, location):
		"""Factory method to get or create a File object from only a filepath.
		Positional arguments:
		location -- The path to the file in the filesystem.
	
		Returns the File object.
		"""
		last_updated, md5 = cls._defaults()
		return cls.objects.get_or_create(location=location, defaults={'last_updated': last_updated, 'md5': md5})

	def open(self, *args, **kwargs):
		"""Wrapper for Python's open() function, called on the given file."""
		pathlib.Path.mkdir(os.path.dirname(self.location), parents=True, exist_ok=True)
		return open(self.location, *args, **kwargs)

	def save(self, *args, **kwargs):
		"""Override save function to update last_updated and md5 before saving."""
		with open(self.location, 'rb') as f:
			text = f.read()

		md5 = hashlib.md5(text).hexdigest()
		if self.md5 != md5:
			self.last_updated = datetime.fromtimestamp(os.path.getmtime(self.location), tz=timezone.utc)
			self.md5 = md5

		return super(File, self).save(*args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	@classmethod
	def _defaults(cls):
		if os.path.exists(location):
			last_updated = datetime.fromtimestamp(os.path.getmtime(filename), tz=timezone.utc)
			with open(location, 'rb') as f:
				md5 = hashlib.md5(f.read()).hexdigest()
		else:
			last_updated = None
			md5 = None

		return last_updated, md5
