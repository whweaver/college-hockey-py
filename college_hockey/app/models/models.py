from django.db import models
from django.utils.translation import gettext_lazy as _

class ChnObject(models.Model):
	chn_path = models.CharField(max_length=255, unique=True)

	class Meta:
		abstract = True

class UschoObject(models.Model):
	uscho_path = models.CharField(max_length=255, unique=True)

	class Meta:
		abstract = True

class Person(ChnObject):
	first_name = models.CharField(max_length=255, null=False)
	last_name = models.CharField(max_length=255, null=False)

	class Meta:
		abstract = True

class DetailedPerson(Person):
	birthdate = models.DateField()
	hometown_city = models.CharField(max_length=255)
	hometown_state = models.CharField(max_length=255)

	class Meta:
		abstract = True

class School(ChnObject):
	pass

class Coach(DetailedPerson):
	alma_mater = models.CharField(max_length=255)
	grad_year = models.CharField(max_length=8)
	teams = models.ManyToManyField('Team')

class Arena(ChnObject):
	name = models.CharField(max_length=255, null=False)
	city = models.CharField(max_length=255)
	state = models.CharField(max_length=255)
	year_built = models.CharField(max_length=4)
	year_closed = models.CharField(max_length=4)
	start_active_year = models.CharField(max_length=4)
	end_active_year = models.CharField(max_length=4)
	capacity = models.PositiveIntegerField()
	sheet_length = models.PositiveSmallIntegerField()
	sheet_width = models.PositiveSmallIntegerField()
	street_addr = models.CharField(max_length=255)

class Player(DetailedPerson):
	teams = models.ManyToManyField('Team', through='Roster')

class Season(ChnObject, UschoObject):
	school_year = models.CharField(max_length=8, unique=True, null=False)

class Referee(Person):
	games = models.ManyToManyField('Game', through='RefereeGame')
	class Meta:
		unique_together = [['first_name', 'last_name']]

class Team(models.Model):
	school = models.ForeignKey('School', on_delete=models.CASCADE, null=False)
	name = models.CharField(max_length=255, null=False)
	season = models.ForeignKey('Season', on_delete=models.CASCADE, null=False)
	nickname = models.CharField(max_length=255)
	conference = models.CharField(max_length=255)
	division = models.PositiveSmallIntegerField()
	wins = models.PositiveSmallIntegerField()
	losses = models.PositiveSmallIntegerField()
	ties = models.PositiveSmallIntegerField()
	reg_season_champs = models.BooleanField()
	conf_tourn_champs = models.BooleanField()
	ncaa_tourn = models.BooleanField()
	has_schedule = models.BooleanField(null=False, default=False)
	arenas = models.ManyToManyField('Arena')
	coaches = models.ManyToManyField('Coach')
	players = models.ManyToManyField('Player', through='Roster')
	games = models.ManyToManyField('Game', through='TeamGame')

	class Meta:
		unique_together = [['school', 'season']]

class Roster(models.Model):
	class Clss(models.TextChoices):
		FRESHMAN = 'Fr', _('Freshman')
		SOPHOMORE = 'So', _('Sophomore')
		JUNIOR = 'Jr', _('Junior')
		SENIOR = 'Sr', _('Senior')
	class Captain(models.TextChoices):
		CAPTAIN = 'C', _('Captain')
		ASSISTANT = 'A', _('Assistant Captain')

	team = models.ForeignKey('Team', on_delete=models.CASCADE, null=False)
	player = models.ForeignKey('Player', on_delete=models.CASCADE, null=False)
	num = models.PositiveSmallIntegerField()
	height = models.PositiveSmallIntegerField()
	weight = models.PositiveSmallIntegerField()
	position = models.CharField(max_length=5)
	clss = models.CharField(max_length=2, choices=Clss.choices)
	captain = models.CharField(max_length=1, choices=Captain.choices)

	class Meta:
		unique_together = [['team', 'player']]

class Game(models.Model):
	class TimeZone(models.TextChoices):
		EASTERN_TIME = 'ET', _('Eastern Time')
		CENTRAL_TIME = 'CT', _('Central Time')
		MOUNTAIN_TIME = 'MT', _('Mountain Time')
		PACIFIC_TIME = 'PT', _('Pacific Time')
		ALASKA_TIME = 'AT', _('Alaska Time')
	team_game_1 = models.ForeignKey('TeamGame', on_delete=models.CASCADE, null=False)
	team_game_2 = models.ForeignKey('TeamGame', on_delete=models.CASCADE, null=False)
	date_time = models.DateTimeField(null=False)
	time_zone = models.CharField(max_length=2, choices=TimeZone.choices)
	chn_box_path = models.CharField(max_length=255)
	chn_metrics_path = models.CharField(max_length=255)
	game_type = models.CharField(max_length=255)
	sked_note = models.CharField(max_length=255)
	box_note = models.CharField(max_length=255)
	ot = models.PositiveSmallIntegerField()
	attendance = models.PositiveIntegerField()
	arena = models.ForeignKey('Arena', on_delete=models.CASCADE)
	season = models.ForeignKey('Season', on_delete=models.CASCADE)
	last_updated = models.DateTimeField(auto_now=True)

class TeamGame(models.Model):
	team = models.ForeignKey('Team', on_delete=models.CASCADE, null=False)
	goals = models.PositiveSmallIntegerField()
	total_blocked_shots = models.PositiveSmallIntegerField()
	total_wide_shots = models.PositiveSmallIntegerField()
	total_hit_posts = models.PositiveSmallIntegerField()
	total_saved_shots = models.PositiveSmallIntegerField()
	total_shot_attempts = models.PositiveSmallIntegerField()
	even_blocked_shots = models.PositiveSmallIntegerField()
	even_wide_shots = models.PositiveSmallIntegerField()
	even_hit_posts = models.PositiveSmallIntegerField()
	even_saved_shots = models.PositiveSmallIntegerField()
	even_shot_attempts = models.PositiveSmallIntegerField()
	pp_blocked_shots = models.PositiveSmallIntegerField()
	pp_wide_shots = models.PositiveSmallIntegerField()
	pp_hit_posts = models.PositiveSmallIntegerField()
	pp_saved_shots = models.PositiveSmallIntegerField()
	pp_shot_attempts = models.PositiveSmallIntegerField()
	close_blocked_shots = models.PositiveSmallIntegerField()
	close_wide_shots = models.PositiveSmallIntegerField()
	close_hit_posts = models.PositiveSmallIntegerField()
	close_saved_shots = models.PositiveSmallIntegerField()
	close_shot_attempts = models.PositiveSmallIntegerField()
	blocks = models.PositiveSmallIntegerField()
	fow = models.PositiveSmallIntegerField()
	fol = models.PositiveSmallIntegerField()

	class Meta:
		unique_together = [['team', 'game']]

class TeamPeriod(models.Model):
	team_game = models.ForeignKey('TeamGame', on_delete=models.CASCADE, null=False)
	period = models.PositiveSmallIntegerField(null=False)
	shots = models.PositiveSmallIntegerField()

	class Meta:
		unique_together = [['team_game', 'period']]

class Goal(models.Model):
	game = models.ForeignKey('Game', on_delete=models.CASCADE, null=False)
	period = models.PositiveSmallIntegerField(null=False)
	game_time = models.DurationField(null=False)
	goal_type = models.CharField(max_length=15)
	scorer = models.ForeignKey('Player', on_delete=models.CASCADE, related_name='goals_scored')
	assists = models.ManyToManyField('Player', related_name='goals_assisted')

	class Meta:
		unique_together = [['game', 'period', 'game_time']]

class Penalty(models.Model):
	player = models.ForeignKey('Player', on_delete=models.CASCADE)
	game = models.ForeignKey('Game', on_delete=models.CASCADE, null=False)
	period = models.PositiveSmallIntegerField(null=False)
	game_time = models.DurationField(null=False)
	duration = models.DurationField(null=False)
	pen_type = models.CharField(max_length=255)

class SkaterGame(models.Model):
	player = models.ForeignKey('Player', on_delete=models.CASCADE, null=False)
	game = models.ForeignKey('Game', on_delete=models.CASCADE, null=False)
	plusminus = models.SmallIntegerField()
	total_blocked_shots = models.PositiveSmallIntegerField()
	total_wide_shots = models.PositiveSmallIntegerField()
	total_hit_posts = models.PositiveSmallIntegerField()
	total_saved_shots = models.PositiveSmallIntegerField()
	total_shot_attempts = models.PositiveSmallIntegerField()
	even_blocked_shots = models.PositiveSmallIntegerField()
	even_wide_shots = models.PositiveSmallIntegerField()
	even_hit_posts = models.PositiveSmallIntegerField()
	even_saved_shots = models.PositiveSmallIntegerField()
	even_shot_attempts = models.PositiveSmallIntegerField()
	pp_blocked_shots = models.PositiveSmallIntegerField()
	pp_wide_shots = models.PositiveSmallIntegerField()
	pp_hit_posts = models.PositiveSmallIntegerField()
	pp_saved_shots = models.PositiveSmallIntegerField()
	pp_shot_attempts = models.PositiveSmallIntegerField()
	close_blocked_shots = models.PositiveSmallIntegerField()
	close_wide_shots = models.PositiveSmallIntegerField()
	close_hit_posts = models.PositiveSmallIntegerField()
	close_saved_shots = models.PositiveSmallIntegerField()
	close_shot_attempts = models.PositiveSmallIntegerField()
	blocks = models.PositiveSmallIntegerField()
	fow = models.PositiveSmallIntegerField()
	fol = models.PositiveSmallIntegerField()

	class Meta:
		unique_together = [['player', 'game']]

class GoalieGame(models.Model):
	player = models.ForeignKey('Player', on_delete=models.CASCADE, null=False)
	game = models.ForeignKey('Game', on_delete=models.CASCADE, null=False)
	total_saves = models.PositiveSmallIntegerField()
	total_goals = models.PositiveSmallIntegerField()
	even_saves = models.PositiveSmallIntegerField()
	even_goals = models.PositiveSmallIntegerField()
	sh_saves = models.PositiveSmallIntegerField()
	sh_goals = models.PositiveSmallIntegerField()
	close_saves = models.PositiveSmallIntegerField()
	close_goals = models.PositiveSmallIntegerField()

	class Meta:
		unique_together = [['player', 'game']]

class RefereeGame(models.Model):
	class Role(models.TextChoices):
		REFEREE = 'ref', _('Referee')
		ASSISTANT = 'asst', _('Assistant Referee')

	referee = models.ForeignKey('Referee', on_delete=models.CASCADE, null=False)
	game = models.ForeignKey('Game', on_delete=models.CASCADE, null=False)
	role = models.CharField(max_length=4, choices=Role.choices)

	class Meta:
		unique_together = [['referee', 'game']]

class PlayerAlias(Person):
	class Meta:
		unique_together = [['chn_path', 'first_name', 'last_name']]

class PlayerEquivalent(models.Model):
	chn_path1 = models.CharField(max_length=255, null=False)
	chn_path2 = models.CharField(max_length=255, null=False)

	class Meta:
		unique_together = [['chn_path1', 'chn_path2']]
