from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnGameMetrics(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, metrics_id, *args, **kwargs):
		"""Factory method to create a game metrics page"""
		return super(GameMetrics, self).get_or_create(f'https://www.collegehockeynews.com/box/metrics.php?gd={metrics_id}', f'chn_school_history_{metrics_id}', *args, **kwargs)

	def refresh(self, *args, **kwargs):
		"""Refresh the game metrics"""
		return super(GameMetrics, self).refresh(self._parse, *args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	def get_metric(self, txt):
		if len(txt) > 0:
			return int(txt)
		else:
			return 0

	def get_skater_row_metrics(self, tr):
		metrics = {}
		for situation, clss in (('total', 'tot'), ('even', 'evs'), ('pp', 'ppl'), ('close', 'cls')):
			metrics[situation] = {}
			for td, result in zip(CSSSelector(f'td.{clss}')(tr), ('blocked_shots', 'wide_shots', 'saved_goals', 'goals', 'shot_attempts')):
				metrics[situation][result] = self.get_metric(td.xpath('string()').strip())
		metrics['blocks'] = get_metric(CSSSelector('td:not([class])')(tr)[0].xpath('string()').strip())
		return metrics

	def _parse(self, html):
		data = {'skaters': [], 'goalies': []}

		for table in CSSSelector('div.widetableWrap table.sortable.metrics')(html):
			skaters = {'skaters': {} }

			# Parse skaters
			for tr in CSSSelector('tbody tr')(table):
				skaters['skaters'][tr[0].xpath('string()')] = get_skater_row_metrics(tr)

			# Parse team totals
			skaters['team'] = get_skater_row_metrics(CSSSelector('tfoot tr')(table)[0])

			data['skaters'].append(skaters)

		# Parse goalie charts
		for table in CSSSelector('table.metrics.sortable[width="50%"]')(html):
			goalies = {}
			for tr in CSSSelector('tbody tr')(table):
				goalies[tr[0].xpath('string()')] = {
					'total_saves': get_metric(tr[1].xpath('string()')),
					'total_goals': get_metric(tr[2].xpath('string()')),
					'even_saves': get_metric(tr[4].xpath('string()')),
					'even_goals': get_metric(tr[5].xpath('string()')),
					'sh_saves': get_metric(tr[7].xpath('string()')),
					'sh_goals': get_metric(tr[8].xpath('string()')),
					'close_saves': get_metric(tr[10].xpath('string()')),
					'close_goals': get_metric(tr[11].xpath('string()'))
				}
			data['goalies'].append(goalies)

		return data
