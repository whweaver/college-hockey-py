from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnSchoolHistory(Webpage):
	chn_path = models.CharField(max_length=255, unique=True)

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, school_id, *args, **kwargs):
		"""Factory method to create the school history"""
		school = super(SchoolHistory, self).get_or_create(f'https://www.collegehockeynews.com/reports/teamHistory/{school_id}', f'chn_school_history_{school_id.replace('/', '.')}', *args, **kwargs)
		school.chn_path = school_id
		return school

	def refresh(self):
		"""Refresh the school history"""
		return super(SchoolHistory, self).refresh(self._parse, self._update_db, redownload=self._is_redownload_required())

	###########################################################################
	# Private methods
	###########################################################################

	def _is_redownload_required(self):
		"""Determine if a redownload is required when refreshing"""
		cur_season = Season.objects.aggregate(Max('school_year'))['school_year']
		return (self.season_id >= cur_season)

	def _parse_coach(self, td):
		if td.xpath('string()').strip() != 'No Coach':
			coach_path = CSSSelector('a')(td)[0].get('href').strip()
		else:
			coach_path = None

		return coach_path

	def _parse_arena(self, td):
		arena_a = CSSSelector('a')(td)[0]
		if len(arena_a.xpath('string()')) > 0:
			arena_path = arena_a.get('href')
		else:
			arena_path = None

	def _parse(self, html):
		data = { 'seasons': [] }

		division = None
		nickname = None
		conference = None
		season = None
		chn_path_id = None
		header_text = CSSSelector('h2.teamlabel')(html)[0].xpath('string()')
		name = re.match(r'(.*)\s*Team\s*History', header_text).group(1).strip()
		for tr in CSSSelector('table.data tbody tr')(html):
			if tr.get('class') == 'stats-section':
				match = re.match(r'(.*)\s*\|\s*Division: (I+) \((.*)\)', tr.xpath('string()'))
				if match is not None:
					nickname = match.group(1).strip()
					division = len(match.group(2))
					conference = match.group(3)
			else:
				tds = CSSSelector('td')(tr)
				if len(tds[0]) > 0:
					a = tds[0][0]
					match = re.match(r'/schedules/team/(.*)/(\d{8})', a.get('href'))
					chn_path_id = match.group(1)
					season = match.group(2)

				data['seasons'].append({
					'name': name,
					'season': season,
					'coach_path': self._parse_coach(tds[5]),
					'arena_path': self._parse_arena(tds[9]),
					'nickname': nickname,
					'conference': conference,
					'wins': int(tds[1].xpath('string()').strip()),
					'losses': int(tds[2].xpath('string()').strip()),
					'ties': int(tds[3].xpath('string()').strip()),
					'chn_path_id': chn_path_id,
					'has_schedule': True,
					'reg_season_champs': ('check' in tds[6].get('class')),
					'conf_tourn_champs': ('check' in tds[7].get('class')),
					'ncaa_tourn': ('check' in tds[8].get('class'))
				})

		return data

	def _update_db(self, data):
		"""Update the database with the season's data"""
		school = School.objects.get_or_create(chn_path=self.chn_path)
		school.save()

		for school_season in data['seasons']:
			season, created = Season.objects.get_or_create(school_year=school_season['season'])
			if created:
				season.save()
			team = Team.objects.get_or_create(school=school, season=season, defaults={
				'name': school_season['name'],
				'nickname': school_season['nickname'],
				'conference': school_season['conference'],
				'wins': school_season['wins'],
				'losses': school_season['losses'],
				'ties': school_season['ties'],
				'reg_season_champs': school_season['reg_season_champs'],
				'conf_tourn_champs': school_season['conf_tourn_champs'],
				'ncaa_tourn': school_season['ncaa_tourn'],
				'has_schedule': school_season['has_schedule']
			})
			team.save()
