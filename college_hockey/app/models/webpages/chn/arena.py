from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnArena(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, arena_id, *args, **kwargs):
		"""Factory method to create the arena"""
		return super(Arena, self).get_or_create(f'https://www.collegehockeynews.com/almanac/arena-detail.php?aid={arena_id}', f'chn_arena_{arena_id}', *args, **kwargs)

	def refresh(self, *args, **kwargs):
		"""Refresh the arena"""
		return super(Arena, self).refresh(self._parse, *args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	def _parse(self, html):
		divs = CSSSelector('div.factbox div.gridinfo div')(html)
		sheet_dims = divs[5].xpath('string()').split('x')
		data = {
			'capacity': int(divs[3].xpath('string()').replace(',', '')),
			'street_addr': divs[7].xpath('string()').split(',')[0],
			'sheet_length': int(sheet_dims[0]),
			'sheet_width': int(sheet_dims[1])
		}

		return data
