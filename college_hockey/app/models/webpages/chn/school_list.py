from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnSchoolList(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, *args, **kwargs):
		"""Factory method to create the school list"""
		return super(SchoolList, self).get_or_create('https://www.collegehockeynews.com/', 'chn_school_list', *args, **kwargs)

	def refresh(self):
		"""Refresh the school list"""
		return super(SchoolList, self).refresh(self._parse, self._update_db, redownload=self._is_redownload_required())

	###########################################################################
	# Private methods
	###########################################################################

	def _is_redownload_required(self):
		"""Determine if a redownload is required when refreshing"""
		now = datetime.datetime.now()
		is_off_season = (now.month > 4)
		cur_season = Season.objects.aggregate(Max('school_year'))['school_year']
		year = now.year
		next_season = f'{year}{year+1}'

		return (is_offseason and (cur_season != next_season))

	def _parse(self, html):
		data = { 'schools': [] }

		for p in CSSSelector('li#teamflyout div div div p')(html):
			path = CSSSelector('a')(p)[0].get('href')
			data['schools'].append({
				'chn_path': path,
				'school_id': re.match(r'https://www\.collegehockeynews\.com/reports/teamHistory/(.*)', path).group(1)
			})

		return data

	def _update_db(self, data):
		"""Update the database with the school list's data"""
		for school in data['schools']:
			chn_school, created = ChnSchoolHistory.get_or_create(school['school_id'])
			if created:
				chn_school.save()
