from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnCoach(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, coach_id, *args, **kwargs):
		"""Factory method to create the coach list"""
		return super(Coach, self).get_or_create(f'https://www.collegehockeynews.com/reports/coach/{coach_id}', f'chn_coach_{coach_id.replace('/', '.')}', *args, **kwargs)

	def refresh(self, *args, **kwargs):
		"""Refresh the coach list"""
		return super(Coach, self).refresh(self._parse, *args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	def _parse(self, html):
		data = {}

		divs = CSSSelector('div.factbox.bio div.gridinfo div')(html)
		divs_1_match = re.match(r'([^\(]*)(?:\((\d{4})\))?', divs[1].xpath('string()'))
		if divs_1_match is not None:
			data['alma_mater'] = divs_1_match.group(1)
			data['grad_year'] = divs_1_match.group(2)
		if re.match(r'^0+\/|\/0+\/', divs[3].xpath('string()')) is not None:
			data['birthdate'] = date(int(re.match(r'\/(\d+)$')[0]), 1, 1)
		else:
			match = re.match(r'\d+/\d+/\d+', divs[3].xpath('string()').strip())
			data['birthdate'] = date(int(match.group(3)), int(match.group(1)), int(match.group(2)))
		hometown_match = re.match(r'([^,]*)(?:,\s*(.*))?', divs[5].xpath('string()'))
		data['hometown_city'] = hometown_match.group(1)
		data['hometown_state'] = hometown_match.group(2)

		return data
