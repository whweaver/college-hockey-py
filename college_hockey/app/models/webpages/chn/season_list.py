from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage
from .season import Season as ChnSeason
from ...models import Season
import datetime

class ChnSeasonList(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls):
		"""Factory method to create the season list"""
		return super(SeasonList, self).get_or_create('https://www.collegehockeynews.com/schedules/', 'chn_season_list')

	def refresh(self):
		"""Refresh the season list"""
		return super(SeasonList, self).refresh(self._parse, self._update_db, redownload=self._is_redownload_required())

	###########################################################################
	# Private methods
	###########################################################################

	def _is_redownload_required(self):
		"""Determine if a redownload is required when refreshing"""
		now = datetime.datetime.now()
		is_off_season = (now.month > 4)
		cur_season = Season.objects.aggregate(Max('school_year'))['school_year']
		year = now.year
		next_season = f'{year}{year+1}'

		return (is_offseason and (cur_season != next_season))

	def _parse(self, html):
		"""Parse the season list"""
		data = { 'seasons': {} }

		for a in CSSSelector('ul.ContextBar li.index.hasmore div ul.linear li a')(html):
			start_year = re.match(r'(\d{4})\D+\d{2}', a.text.strip()).group(1)
			years = f'{start_year}{int(start_year)+1}'
			data['seasons'][years] = a.get('href')

		return data

	def _update_db(self, data):
		"""Update the database with the season list's data"""
		for season_id in data['seasons']:
			chn_season, created = ChnSeason.get_or_create(season_id)
			if created:
				chn_season.save()
