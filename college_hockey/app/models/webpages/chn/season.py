from lxml.cssselect import CSSSelector
import re
from datetime import datetime, time
from ...webpage import Webpage
from ...models import Season

class ChnSeason(Webpage):
	season_id = models.CharField(max_length=8, unique=True, null=False)

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, season_id, *args, **kwargs):
		"""Factory method to create the season"""
		season = super(Season, self).get_or_create(f'https://www.collegehockeynews.com/schedules/?season={season_id}', f'chn_season_{season_id}', *args, **kwargs)
		season.season_id = season_id
		return season

	def refresh(self):
		"""Refresh the season"""
		return super(Season, self).refresh(self._parse, self._update_db, redownload=self._is_redownload_required())

	###########################################################################
	# Private methods
	###########################################################################

	def _is_redownload_required(self):
		"""Determine if a redownload is required when refreshing"""
		cur_season = Season.objects.aggregate(Max('school_year'))['school_year']
		return (self.season_id >= cur_season)

	def _parse_season_id(self, html):
		h1_text = CSSSelector('div#wrap div#content h1')(html)[0].text
		match = re.match(r'(\d{4})\D+\d{2}', h1_text)
		start_year = int(match.group(1))
		return f'{start_year}{start_year + 1}'

	def _parse_team(self, td):
		team = {}

		team['name'] = td.xpath('string()').strip()
		if len(td) > 0:
			team['path'] = re.match(r'reports/team/(.*)', td[0].get('href')).group(1)

		return team

	def _parse_goals(self, td):
		goals_str = td.text
		if goals_str is not None:
			return int(goals_str)
		else:
			return goals_str

	def _parse_score(self, tds):
		return [
			{
				'team': self._parse_team(tds[0]),
				'goals': self._parse_goals(tds[1])
			},
			{
				'team': self._parse_team(tds[3]),
				'goals': self._parse_goals(tds[4])
			}
		]

	def _parse_ot(self, td):
		ot = None
		ot_str = td.xpath('string()')
		match = re.match(r'(\d+)?ot', ot_str)
		if match is not None:
			ot_str = match.group(1)
			if ot_str is not None:
				ot = int(ot_str)
			else:
				ot = 1
		else:
			ot = 0

		return ot

	def _parse_game(self, tr, game_type, game_date):
		game = { 'type': game_type }
		tds = CSSSelector('td')(tr)

		num_tds = len(tds)
		if num_tds == 10:
			match = re.match(r'(\d+):0*(\d+)\s*([^\s\u00A0]+)', tds[6].text)
			if match is not None:
				hour = int(match.group(1))
				if hour < 12:
					hour += 12
				minute = int(match.group(2))
				game['datetime'] = datetime.combine(game_date, time(hour, minute))
				game['timezone'] = match.group(3)

			game['note'] = tds[9].xpath("string()").strip()

			box_td = tds[7]
			if len(box_td) > 0:
				box_href = box_td[0].get('href')
				if re.match(r'/box/final/', box_href):
					game['box_path'] = box_href

			metrics_td = tds[8]
			if len(metrics_td) > 0:
				metrics_href = metrics_td[0].get('href')
				if re.match(r'/box/metrics\.php\?gd=', metrics_href):
					game['metrics_path'] = metrics_href
		elif num_tds == 8:
			game['note'] = tds[7].xpath("string()").strip()

		else:
			raise Exception(f'Unknown schedule line format (num_tds = {num_tds})!')

		game['score'] = self._parse_score(tds)
		game['at'] = tds[2].text
		game['ot'] = self._parse_ot(tds[5])

		return game

	def _parse_games(self, html, season_id):
		games = []
		game_date = None
		game_type = None

		for tr in CSSSelector('table.data.schedule.full tbody tr:not(.empty)')(html):
			tr_class = tr.get('class')
			if tr_class == 'stats-section':
				game_date = datetime.strptime(tr.xpath("string()").strip(), '%A, %B %d, %Y')
			elif tr_class == 'sked-header':
				game_type = tr.xpath("string()").strip()
			else:
				games.append(self._parse_game(tr, game_date, game_type))

		return games

	def _parse(self, html):
		data = {}

		data['season_id'] = self._parse_season_id(html)
		data['games'] = self._parse_games(html, data['season_id'])

		return data

	def _update_db(self, data):
		"""Update the database with the season's data"""
		season, created = Season.objects.get_or_create(school_year=data['season_id'])
		if created:
			season.save()

		for game_dict in data['games']:

			# Get or create the Team objects and save their corresponding scores
			scores = {}
			teams = []
			for score_dict in game_dict['score']:
				team_dict = score_dict['team']
				score = score_dict['goals']
				with transaction.atomic():
					if 'chn_path' in team_dict:
						school = School.objects.get(chn_path=chn_path)
					else:
						school, created = School.objects.get_or_create(chn_path=None, team__name=team_dict['name'])
						if created:
							school.save()
					team, created = Team.objects.get_or_create(season=season, school=school, defaults={
						'name': team_dict['name']
					})
					if created:
						team.save()
					teams.append(team)
					scores[team] = score

			# Get or create the Game object
			game, created = Game.objects.get_or_create(date_time=game_dict['datetime'], time_zone=game_dict['timezone'], team_game_1__team=teams[0], team_game_2__team=teams[1], defaults={
				'chn_box_path': game_dict['box_path'],
				'chn_metrics_path': game_dict['metrics_path'],
				'game_type': game_dict['game_type'],
				'sked_note': game_dict['note'],
				'ot': game_dict['ot'],
				'season': season
			})
			if created:
				game.save()

			# Get or create the TeamGame objects
			for team in scores:
				team_game, created = TeamGame.objects.get_or_create(team=team, game=game, defaults={
					'goals': scores[team]
				})
				if created:
					team_game.save()
