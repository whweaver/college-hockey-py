from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnCoachList(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, *args, **kwargs):
		"""Factory method to create the coach list"""
		return super(CoachList, self).get_or_create('https://www.collegehockeynews.com/almanac/coach-alltime.php', 'chn_coach_list', *args, **kwargs)

	def refresh(self):
		"""Refresh the coach list"""
		return super(CoachList, self).refresh(self._parse, self._update_db, redownload=self._is_redownload_required())

	###########################################################################
	# Private methods
	###########################################################################

	def _is_redownload_required(self):
		"""Determine if a redownload is required when refreshing"""
		return True

	def _parse(self, html):
		data = []

		for tr in CSSSelector('table.stats.data.sortable tbody tr')(html):
			tds = CSSSelector('td')(tr)
			name = tds[1].xpath('string()').split(',')
			data.append({
				'first_name': name[1].strip(),
				'last_name': name[0].strip(),
				'chn_path': CSSSelector('a')(tds[1])[0].get('href')
			})

		return data

	def _update_db(self, data):
		"""Update the database with the coach list's data"""
		for coach_info in data:
			pass
