from lxml.cssselect import CSSSelector
import re
from datetime import timedelta
from ...webpage import Webpage

class ChnGameBox(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, box_id, *args, **kwargs):
		"""Factory method to create the game box page"""
		return super(GameBox, self).get_or_create(f'https://www.collegehockeynews.com/box/final/{box_id}', f'chn_game_box_{box_id.replace('/', '.')}', *args, **kwargs)

	def refresh(self, *args, **kwargs):
		"""Refresh the game box"""
		return super(GameBox, self).refresh(self._parse, *args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	def _parse_arena(self, subtext):
		arena_text = subtext[1].xpath('string()')
		if 'shootout' in arena_text and not re.match(r'\s+at\s+'):
			arena_text = subtext[2].xpath('string()')

		match = re.match(r'.*Game\s*(.*)\s+at\s+.*\s+at\s+(\S.*),\s*(\S.*),\s*(\S.*)', arena_text)
		if match is not None:
			print('here1')
			return {
				'name': match.group(2).strip(),
				'city': match.group(3).strip(),
				'state': match.group(4).strip()
			}
		
		match = re.match(r'.*Game\s*(.*)\s+at\s+(\S.*\s+at\s+\S.*),\s*(\S.*)', arena_text)
		if match is not None:
			print('here2')
			name = match.group(2)
			at_match = re.match(r'(.*\S)\s+at\s+(\S.*)', name)
			first_at = at_match.group(2)
			second_at = at_match.group(3)
			if first_at == second_at:
				return {
					'name': second_at,
					'city': match.group(3).strip(),
					'state': None
				}
			else:
				return {
					'name': name,
					'city': match.group(3).strip(),
					'state': None
				}

		match = re.match(r'.*Game\s*(.*)\s+at\s+(\S.*),\s*(\S.*)\s*at\s+(\S.*),', arena_text)
		if match is not None:
			print('here3')
			return {
				'name': match.group(4).strip(),
				'city': match.group(2).strip(),
				'state': match.group(3).strip()
			}

		match = re.match(r'.*Game\s*(.*)\s+at\s+(\S.*),\s*(\S.*),\s*(\S.*)', arena_text)
		if match is not None:
			print('here4')
			return {
				'name': match.group(2).strip(),
				'city': match.group(3).strip(),
				'state': match.group(4).strip()
			}

		match = re.match(r'.*Game\s*(.*)\s+at\s+(\S[^\(]*),\s*(\S.*)', arena_text)
		if match is not None:
			print('here5')
			return {
				'name': match.group(2).strip(),
				'city': match.group(3).strip(),
				'state': None
			}

		match = re.match(r'.*Game\s*(.*)\s+at\s+(.*),', arena_text)
		if match is not None:
			print('here6')
			return {
				'name': match.group(2).strip(),
				'city': None,
				'state': None
			}

		raise Exception(f'Cannot parse the following text:\n{arena_text}')

	def _parse_meta(self, html):
		data = {}
		subtext = CSSSelector('div#meta')(html)[0][2]
		data['arena'] = self._parse_arena(subtext)
		data['referees'] = []
		match = re.match(r'Referees?: (?:(.*),\s*)*(.*)\s*(Asst. Referees?:) (?:(.*),\s*)*(.*)', subtext[2].xpath('string()'))
		if match is not None:
			assistant = False
			for group in match.groups():
				if re.match(r'Asst. Referees?:', group):
					assistant = True
				elif group is not None:
					names = group.split(None, 2)
					data['referees'].append({
						'first_name': names[0],
						'last_name': names[1],
						'role': 'asst' if assistant else 'ref'
					})
		return data

	def _parse_linescore(self, html):
		data = {'shots': {}}
		for tr in CSSSelector('div#linescore div#shots table tbody tr')(html):
			team = tr[1].xpath('string()')
			data['shots'][team] = []
			for td in tr[1:-2]:
				data['shots'][team].append(int(td.xpath('string()')))
		return data

	def _parse_game_time(self, text):
		match = re.match(r'(\d+):(\d+)', text)
		return timedelta(minutes=int(match.group(1)), seconds=int(match.group(2)))

	def _parse_scoring(self, html):
		data = {'goals': []}
		period = None
		for tr in CSSSelector('div#scoring table tr')(html):

			# Found new period
			if tr.get('class') == 'stats-section':
				period_text = tr[0].xpath('string()')
				match = re.match(r'^(\d+).*Period$', period_text)
				if match is not None:
					period = int(match.group(1))
				elif period_text == 'Overtime':
					period = 4

			# Found new goal
			elif re.match(r'\wscore', tr.get('class')):
				# Extract team abbreviation
				team_abbr = tr[0].xpath('string()').strip()

				# Extract scorer's CHN path
				scorer_a = tr[3][0]
				scorer_path = scorer_a.get('href')

				# Extract assist names
				assists = []
				match = re.match(r'(?:([^,]*),\s*)*([^,]*)', tr[4].xpath('string()'))
				for group in match.groups():
					if group is not None:
						assists.append(group.strip())

				data['goals'].append({
					'period': period,
					'game_time': self._parse_game_time(tr[5].xpath('string()')),
					'type': tr[1].xpath('string()').strip(),
					'scorer_path': scorer_path,
					'assists': assists
				})

		return data

	def _parse_penalties(self, html):
		data = {'penalties': []}
		period = None
		for tr in CSSSelector('div#penalties table tr')(html):

			# Found new period
			if tr.get('class') == 'stats-section':
				match = re.match(r'^(\d+).*Period$', tr[0].xpath('string()').strip())
				if match is not None:
					period = int(match.group(1))

				elif re.match(r'\wscore', tr.get('class')):
					player_name = tr[1].xpath('string()').strip()
					data['penalties'].append({
						'player': tr[1].xpath('string()').strip(),
						'duration': int(tr[2].xpath('string()').strip()),
						'type': tr[3].xpath('string()').strip(),
						'game_time': self._parse_game_time(tr[4].xpath('string()'))
					})

		return data

	def _parse_goalies(self, html):
		data = {'goalies': {}}
		for goalie_table in CSSSelector('div#goalies table tbody')(html):
			for tr in goalie_table:
				data['goalies'][tr[0].xpath('string()').strip()] = {
					'total_saves': int(tr[1].xpath('string()')),
					'total_goals': int(tr[2].xpath('string()'))
				}
		return data

	def _parse_skaters(self, html):
		data = {'skaters': {}}
		for player_table in CSSSelector('div#playersums div.playersum table.data')(html):
			for tr in CSSSelector('tbody tr')(player_table):
				player_name = tr[0].xpath('string()').strip()

				fo_str = tr[7].xpath('string()').strip()
				if len(fo_str) == 0:
					fow = None
					fol = None
				else:
					match = re.match(r'(\d+)(?:\D.*\D|\D)(\d+)', fo_str)
					if match is not None:
						fow = int(match.group(1))
						fol = int(match.group(2))
					else:
						fow = 0
						fol = 0

				data['skaters'][player_name] = {
					'plusminus': int(tr[4].xpath('string()').strip()),
					'fow': fow,
					'fol': fol
				}
		return data

	def _parse(self, html):
		data = {}

		if CSSSelector('div#content h2')(html)[0].xpath('string()') != 'Game Not Available':
			data['meta'] = self._parse_meta(html)
			data['linescore'] = self._parse_linescore(html)
			data['scoring'] = self._parse_scoring(html)
			data['penalties'] = self._parse_penalties(html)
			data['goalies'] = self._parse_goalies(html)
			data['skaters'] = self._parse_skaters(html)

		return data

	def _update_db(self, data):
		
