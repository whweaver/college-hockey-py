from lxml.cssselect import CSSSelector
import re
from ...webpage import Webpage

class ChnArenaList(Webpage):

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def get_or_create(cls, *args, **kwargs):
		"""Factory method to create the arena list"""
		return super(ArenaList, self).get_or_create('https://www.collegehockeynews.com/almanac/arenas-index.php', 'arena_list', *args, **kwargs)

	def refresh(self, *args, **kwargs):
		"""Refresh the arena list"""
		return super(ArenaList, self).refresh(self._parse, *args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################

	def _parse(self, html):
		data = {'arenas': []}

		for tr in CSSSelector('table.data.sortable tbody tr')(html):
			tds = CSSSelector('td')(tr)
			location_str = tds[1].xpath('string()')
			if len(location_str) > 0:
				match = re.match(r'([^,]*)(?:,\s*(.*))?')
				city = match.group(1)
				state = match.group(2)
			else:
				city = None
				state = None
			active_years = tds[4].xpath('string()').split('-')

			data['arenas'].append({
				'year_built': tds[2].xpath('string()'),
				'year_closed': tds[3].xpath('string()'),
				'city': city,
				'state': state,
				'start_active_year': active_years[0],
				'end_active_year': active_years[1],
				'chn_path': CSSSelector('a')(tds[0])[0].get('href')
			})

		return data
