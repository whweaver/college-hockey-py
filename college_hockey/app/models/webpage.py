from django.db import models
from djanog.db import transaction
import requests
from lxml import etree
import json

class Webpage(models.Model):
	url = models.CharField(max_length=255, null=False, unique=True)
	text_cache = models.ForeignKey('File', null=False, unique=True)
	data_cache = models.ForeignKey('File', null=False, unique=True)
	last_download = models.DateTimeField(null=True)
	db_last_updated = models.DateTimeField(null=True)

	###########################################################################
	# Public methods
	###########################################################################

	@classmethod
	def create(cls, url, filename):
		"""Factory method to create a Webpage object from only a URL."""
		text_cache, data_cache = cls._defaults(filename)
		return cls(url=url, text_cache=text_cache, data_cache=data_cache)

	@classmethod
	def get_or_create(cls, url, filename):
		text_cache, data_cache = cls._defaults(filename)
		return cls.objects.get_or_create(url=url, defaults={'text_cache': text_cache, 'data_cache': data_cache})

	def refresh(self, parse, update_db=None, redownload=True):
		"""Refresh the webpage and reparse if necessary."""
		if redownload or self.text_cache is None:
			self._refresh_text_cache()
			
		if (self.data_cache.last_updated is None) or (self.text_cache.last_updated > self.data_cache.last_updated):
			self._refresh_data_cache(parse)

		if (update_db is not None) and ((self.db_last_updated is None) or (self.data_cache.last_updated > self.db_last_updated)):
			with transaction.atomic():
				update_db()
			self.db_last_updated = datetime.fromtimestamp(os.path.getmtime(self.location), tz=timezone.utc)

	def data(self):
		"""Retrieve the data from the file"""
		serialized = None
		with self.data_cache.open('r') as f:
			serialized = f.read()
		return json.load(serialized)

	def save(self, *args, **kwargs):
		"""Auto-save the associated cache entries when saving the webpage"""
		with transaction.atomic():
			self.text_cache.save()
			self.data_cache.save()
			super(Webpage, self).save(*args, **kwargs)

	###########################################################################
	# Private methods
	###########################################################################
	
	def _refresh_text_cache(self):
		"""Re-downloads the webpage and saves its text to the cache."""
		page = requests.get(self.url)
		with self.text_cache.open('w') as f:
			f.write(page)
		self.last_download = datetime.fromtimestamp(os.path.getmtime(self.location), tz=timezone.utc)

	def _refresh_data_cache(self, parse):
		"""Re-parses the (previously downloaded) webpage and saves its data to the cache.
		Positional arguments:
		parse -- A function that accepts an lxml etree object and returns a JSON-able object.
		"""

		# Read page text from text cache
		page = None
		with self.text_cache.open('r') as f:
			page = f.read()

		# Parse data from text and serialize it
		data = parse(etree.HTML(page))
		serialized = json.dumps(data)

		# Write serialized data to data cache
		with self.data_cache.open('w') as f:
			f.write(serialized)

	@classmethod
	def _defaults(cls, filename):
		"""Defaults when creating a new webpage"""
		mod_name = cls.__module__
		cls_name = cls.__qualname__
		if mod_name is not None and mod_name != '__builtin__':
			cls_name = f'{mod_name}.{cls_name}'
		text_cache = File.create(f'cache/text/{cls_name}/{filename}.html')
		data_cache = File.create(f'cache/data/{cls_name}/{filename}.json')
		return text_cache, data_cache
