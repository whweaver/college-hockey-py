from models.webpages.chn.season_list import ChnSeasonList
from models.webpages.chn.season import ChnSeason
import concurrent.futures
import datetime
from models.models import *

class Updater:
	def __init__(self):
		self.__executor = concurrent.futures.ProcessPoolExecutor()

	def __del__(self):
		self.__executor.shutdown(cancel_futures=True)

	def update_season_list(self):
		"""Update the season list"""
		season_list = ChnSeasonList.get_or_create()
		season_list.refresh()
		season_list.save()

	def update_school_list(self):
		"""Update the school list"""
		school_list = ChnSchoolList.get_or_create()
		school_list.refresh()
		school_list.save()

	def update_season(self, chn_season):
		"""Update the given season"""
		chn_season.refresh()
		chn_season.save()

	def update_school_history(self, school_history):
		"""Update the given school's history"""
		school_history.refresh()
		school_history.save()

	def _update_async(self):
		# School list and season list can both be done immediately and asynchronously
		school_list_future = self.__executor.submit(self.update_school_list)
		season_list_future = self.__executor.submit(self.update_season_list)

		# Need to wait for school list to complete before doing school histories
		school_list_future.wait()
		self.__executor.map(self.update_school_history, ChnSchoolHistory.objects.all())

		# Need to wait for season list AND school histories to complete before doing seasons
		season_list_future.wait()
		self.__executor.map(self.update_season, ChnSeason.objects.all())

	# Update all CHN data asynchronously.
	# @return A Future object that resolves when everything is updated.
	def update():
		return self.__executor.submit(self._update_async)
